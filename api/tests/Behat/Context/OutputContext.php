<?php

namespace App\Tests\Behat\Context;

use Behat\Behat\Context\Context;
use Behat\Behat\Hook\Scope\AfterScenarioScope;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Behat\Testwork\Tester\Result\TestResult;
use Exception;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\DomCrawler\Crawler;

class OutputContext implements Context
{
    private $output;
    private $useFancyExceptionReporting = true;
    private $requestContext;

    /**
     * @param BeforeScenarioScope $scope
     *
     * @BeforeScenario
     */
    public function gatherContexts(BeforeScenarioScope $scope)
    {
        $environment = $scope->getEnvironment();

        $this->requestContext = $environment->getContext(RequestContext::class);
    }

    /**
     * @Given /^print last response$/
     * @throws Exception
     */
    public function printLastResponse()
    {
        if ($this->requestContext->lastResponse) {
            // Build the first line of the response (protocol, protocol version, status code, reason phrase)
            $response = 'HTTP/1.1 ' . $this->requestContext->lastResponse->getStatusCode() . "\r\n";

            // Add the headers
            foreach ($this->requestContext->lastResponse->getHeaders() as $key => $value) {
                $response .= sprintf("%s: %s\r\n", $key, $value[0]);
            }

            // Add the response body
            $response .= $this->prettifyJson($this->requestContext->lastResponse->getContent(false));

            // Print the response
            $this->printDebug($response);
        }
    }

    /**
     * @AfterScenario
     * @param AfterScenarioScope $scope
     * @throws Exception
     */
    public function printLastResponseOnError(AfterScenarioScope $scope): void
    {
        if ($scope->getTestResult()->getResultCode() === TestResult::FAILED) {
            if ($this->requestContext->lastResponse === null) {
                return;
            }

            $body = $this->requestContext->lastResponse->getContent(false);

            $this->printDebug('');
            $this->printDebug('<error>Failure!</error> when making the following request:');
            $this->printDebug(sprintf('<comment>%s</comment>: <info>%s</info>', $this->requestContext->lastRequest->getMethod(), $this->requestContext->lastRequest->getUri()) . "\n");

            if (in_array($this->requestContext->lastResponse->getHeaders()['content-type'], ['application/json', 'application/problem+json'])) {
                $this->printDebug($this->prettifyJson($body));
            } else {
                // the response is HTML - see if we should print all of it or some of it
                $isValidHtml = strpos($body, '</body>') !== false;

                if ($this->useFancyExceptionReporting && $isValidHtml) {
                    $this->printDebug('<error>Failure!</error> Below is a summary of the HTML response from the server.');

                    // finds the h1 and h2 tags and prints them only
                    $crawler = new Crawler($body);
                    foreach ($crawler->filter('h1, h2')->extract(array('_text')) as $header) {
                        $this->printDebug(sprintf('        ' . $header));
                    }
                } else {
                    $this->printDebug($body);
                }
            }
        }
    }

    /**
     * Returns the prettified equivalent of the input if the input is valid JSON.
     * Returns the original input if it is not valid JSON.
     *
     * @param $input
     *
     * @return string
     * @throws Exception
     */
    public function prettifyJson($input): string
    {
        $decodedJson = json_decode($input);

        if ($decodedJson === null) { // JSON is invalid
            return $input;
        }

        return json_encode($decodedJson, JSON_PRETTY_PRINT);
    }


    /**
     * @param $string
     */
    public function printDebug($string): void
    {
        $this->getOutput()->writeln($string);
    }


    /**
     * @return ConsoleOutput
     */
    public function getOutput(): ConsoleOutput
    {
        if ($this->output === null) {
            $this->output = new ConsoleOutput();
        }

        return $this->output;
    }
}

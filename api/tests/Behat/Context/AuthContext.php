<?php

namespace App\Tests\Behat\Context;

use Behat\Behat\Context\Context;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class AuthContext implements Context
{
    /**
     * The user to use with HTTP basic authentication
     *
     * @var string
     */
    public $authUser;

    /**
     * The password to use with HTTP basic authentication
     *
     * @var string
     */
    public $authPassword;

    /**
     * The JWT token of the current user
     *
     * @var string
     */
    public $bearer;

    /** @var JWTTokenManagerInterface */
    private $JWTManager;

    /** @var AuthenticationManagerInterface */
    private $authenticationManager;

    public function __construct(
        JWTTokenManagerInterface $JWTManager,
        AuthenticationManagerInterface $authenticationManager
    )
    {
        $this->JWTManager = $JWTManager;
        $this->authenticationManager = $authenticationManager;
    }

    /**
     * @Given /^I authenticate with user "([^"]*)" and password "([^"]*)"$/
     * @param $email
     * @param $password
     */
    public function iAuthenticateWithEmailAndPassword($email, $password): void
    {
        $this->authUser = $email;
        $this->authPassword = $password;

        $unauthenticatedToken = new UsernamePasswordToken(
            $email,
            $password,
            "main"
        );

        $authenticatedToken = $this
            ->authenticationManager
            ->authenticate($unauthenticatedToken);

        $user = $authenticatedToken->getUser();

        $this->bearer = $this->JWTManager->create($user);
    }
}



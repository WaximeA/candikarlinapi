<?php

namespace App\Tests\Behat\Context;

use ApiPlatform\Core\Api\IriConverterInterface;
use Behat\Behat\Context\Context;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Behat\Gherkin\Node\TableNode;
use Fidry\AliceDataFixtures\Loader\PersisterLoader;
use Fidry\AliceDataFixtures\ProcessorInterface;
use Symfony\Component\HttpKernel\KernelInterface;

class FixtureContext implements Context, ProcessorInterface
{
    /**
     * @var PersisterLoader
     */
    private $fixtureLoader;

    /** @var ReferenceContext */
    private $refContext;

    public function __construct(IriConverterInterface $iriConverter, KernelInterface $kernel)
    {
        $this->fixtureLoader = $kernel->getContainer()->get('fidry_alice_data_fixtures.loader.doctrine');
    }

    /**
     * @param BeforeScenarioScope $scope
     *
     * @BeforeScenario
     */
    public function gatherContexts(BeforeScenarioScope $scope): void
    {
        $environment = $scope->getEnvironment();

        $this->refContext = $environment->getContext(ReferenceContext::class);
    }

    /**
     * @Given /^the fixtures file "([^"]*)" is loaded$/
     * @param string $file
     */
    public function theFixturesFileIsLoaded(string $file): void
    {
        $this->load(['./fixtures/' . $file . '.yml']);
    }

    /**
     * @Given the following fixtures files are loaded:
     * @param TableNode $table
     */
    public function theFixturesFilesAreLoaded(TableNode $table): void
    {
        $files = array_map(fn($row) => './fixtures/' . $row[0] . '.yaml', $table->getRows());

        $objects = $this->load($files);
    }

    /**
     * Load Fixtures
     *
     * @param array $files
     * @return array|object[]
     */
    private function load(array $files): iterable
    {
        return $this->fixtureLoader->load($files);
    }

    /**
     * @param string $fixtureId
     * @param object $object
     */
    public function preProcess(string $fixtureId, $object): void
    {
        // do nothing now
    }

    /**
     * @param string $fixtureId
     * @param object $object
     */
    public function postProcess(string $fixtureId, $object): void
    {
        $this->refContext->addReference($fixtureId, $object);
        // do nothing now
    }
}

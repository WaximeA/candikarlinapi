<?php

namespace App\Tests\Behat\Context;

use App\Tests\Behat\Context\Traits\{TransactionalDatabaseTrait, UtilsTrait};
use Behat\Behat\Context\Context;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Behat\Gherkin\Node\PyStringNode;
use Exception;
use InvalidArgumentException;
use PHPUnit\Framework\Assert;

class ApiFeatureContext implements Context
{
    use UtilsTrait, TransactionalDatabaseTrait;

    /** @var RequestContext */
    private $requestContext;

    /** @var ScopeContext */
    private $scopeContext;

    /**
     * @param BeforeScenarioScope $scope
     *
     * @BeforeScenario
     */
    public function gatherContexts(BeforeScenarioScope $scope)
    {
        $environment = $scope->getEnvironment();

        $this->requestContext = $environment->getContext(RequestContext::class);
        $this->scopeContext = $environment->getContext(ScopeContext::class);
    }

    /**
     * @Then /^the "([^"]*)" property should equal "([^"]*)"$/
     * @param $property
     * @param $expectedValue
     * @throws Exception
     */
    public function thePropertyEquals($property, $expectedValue): void
    {
        $payload = $this->requestContext->getScopePayload();
        $actualValue = self::arrayGet($payload, $property);

        Assert::assertEquals(
            $expectedValue,
            $actualValue,
            "Asserting the [$property] property in current scope equals [$expectedValue]: " . json_encode($payload)
        );
    }

    /**
     * @Then /^the "([^"]*)" property should contain "([^"]*)"$/
     * @param $property
     * @param $expectedValue
     * @throws Exception
     */
    public function thePropertyShouldContain($property, $expectedValue): void
    {
        $payload = $this->requestContext->getScopePayload();
        $actualValue = self::arrayGet($payload, $property);

        // if the property is actually an array, use JSON so we look in it deep
        $actualValue = is_array($actualValue) ? json_encode($actualValue, JSON_PRETTY_PRINT) : $actualValue;
        Assert::assertContains(
            $expectedValue,
            $actualValue,
            "Asserting the [$property] property in current scope contains [$expectedValue]: " . json_encode($payload)
        );
    }

    /**
     * @Given /^the "([^"]*)" property should not contain "([^"]*)"$/
     * @param $property
     * @param $expectedValue
     * @throws Exception
     */
    public function thePropertyShouldNotContain($property, $expectedValue): void
    {
        $payload = $this->requestContext->getScopePayload();
        $actualValue = self::arrayGet($payload, $property);

        // if the property is actually an array, use JSON so we look in it deep
        $actualValue = is_array($actualValue) ? json_encode($actualValue, JSON_PRETTY_PRINT) : $actualValue;
        Assert::assertNotContains(
            $expectedValue,
            $actualValue,
            "Asserting the [$property] property in current scope does not contain [$expectedValue]: " . json_encode($payload)
        );
    }

    /**
     * @Then /^the "([^"]*)" property should exist$/
     * @param $property
     * @throws Exception
     */
    public function thePropertyExists($property): void
    {
        $payload = $this->requestContext->getScopePayload();

        $message = sprintf(
            'Asserting the [%s] property exists in the scope [%s]: %s',
            $property,
            $this->scopeContext->scope,
            json_encode($payload)
        );

        Assert::assertTrue(self::arrayHas($payload, $property), $message);
    }

    /**
     * @Then /^the "([^"]*)" property should not exist$/
     * @param $property
     * @throws Exception
     */
    public function thePropertyDoesNotExist($property): void
    {
        $payload = $this->requestContext->getScopePayload();

        $message = sprintf(
            'Asserting the [%s] property does not exist in the scope [%s]: %s',
            $property,
            $this->scopeContext->scope,
            json_encode($payload)
        );

        Assert::assertFalse(self::arrayHas($payload, $property), $message);
    }

    /**
     * @Then /^the "([^"]*)" property should be an array$/
     * @param $property
     * @throws Exception
     */
    public function thePropertyIsAnArray($property): void
    {
        $payload = $this->requestContext->getScopePayload();

        $actualValue = self::arrayGet($payload, $property);

        Assert::assertIsArray(
            $actualValue, "Asserting the [$property] property in current scope [{$this->scopeContext->scope}] is an array: " . json_encode($payload)
        );
    }

    /**
     * @Then /^the "([^"]*)" property should be an object$/
     * @param $property
     * @throws Exception
     */
    public function thePropertyIsAnObject($property): void
    {
        $payload = $this->requestContext->getScopePayload();

        $actualValue = self::arrayGet($payload, $property);

        Assert::assertIsObject(
            $actualValue, "Asserting the [$property] property in current scope [{$this->scopeContext->scope}] is an object: " . json_encode($payload)
        );
    }

    /**
     * @Then /^the "([^"]*)" property should be an empty array$/
     * @param $property
     * @throws Exception
     */
    public function thePropertyIsAnEmptyArray($property): void
    {
        $payload = $this->requestContext->getScopePayload();
        $scopePayload = self::arrayGet($payload, $property);

        Assert::assertTrue(
            is_array($scopePayload) and $scopePayload === array(),
            "Asserting the [$property] property in current scope [{$this->scopeContext->scope}] is an empty array: " . json_encode($payload)
        );
    }

    /**
     * @Then /^the "([^"]*)" property should contain (\d+) item(?:|s)$/
     * @param $property
     * @param $count
     * @throws Exception
     */
    public function thePropertyContainsItems($property, $count): void
    {
        $payload = $this->requestContext->getScopePayload();

        Assert::assertCount(
            $count,
            self::arrayGet($payload, $property),
            "Asserting the [$property] property contains [$count] items: " . json_encode($payload)
        );
    }

    /**
     * @Then /^the "([^"]*)" property should be an integer$/
     * @param $property
     * @throws Exception
     */
    public function thePropertyIsAnInteger($property): void
    {
        $payload = $this->requestContext->getScopePayload();

        Assert::assertIsInt(
            self::arrayGet($payload, $property),
            "Asserting the [$property] property in current scope [{$this->scopeContext->scope}] is an integer: " . json_encode($payload)
        );
    }

    /**
     * @Then /^the "([^"]*)" property should be a string$/
     * @param $property
     * @throws Exception
     */
    public function thePropertyIsAString($property): void
    {
        $payload = $this->requestContext->getScopePayload();

        Assert::assertIsString(
            self::arrayGet($payload, $property, true),
            "Asserting the [$property] property in current scope [{$this->scopeContext->scope}] is a string: " . json_encode($payload)
        );
    }

    /**
     * @Then /^the "([^"]*)" property should be a string equalling "([^"]*)"$/
     * @param $property
     * @param $expectedValue
     * @throws Exception
     */
    public function thePropertyIsAStringEqualling($property, $expectedValue): void
    {
        $payload = $this->requestContext->getScopePayload();

        $this->thePropertyIsAString($property);

        $actualValue = self::arrayGet($payload, $property);

        Assert::assertSame(
            $actualValue,
            $expectedValue,
            "Asserting the [$property] property in current scope [{$this->scopeContext->scope}] is a string equalling [$expectedValue]."
        );
    }

    /**
     * @Then /^the "([^"]*)" property should be a boolean$/
     * @param $property
     * @throws Exception
     */
    public function thePropertyIsABoolean($property): void
    {
        $payload = $this->requestContext->getScopePayload();

        Assert::assertEquals(
            gettype(self::arrayGet($payload, $property)), 'boolean', "Asserting the [$property] property in current scope [{$this->scopeContext->scope}] is a boolean."
        );
    }

    /**
     * @Then /^the "([^"]*)" property should be a boolean equalling "([^"]*)"$/
     * @param $property
     * @param $expectedValue
     * @throws Exception
     */
    public function thePropertyIsABooleanEqualling($property, $expectedValue): void
    {
        $payload = $this->requestContext->getScopePayload();
        $actualValue = self::arrayGet($payload, $property);

        if (!in_array($expectedValue, array('true', 'false'))) {
            throw new InvalidArgumentException("Testing for booleans must be represented by [true] or [false].");
        }

        $this->thePropertyIsABoolean($property);

        Assert::assertSame(
            $actualValue,
            $expectedValue === 'true',
            "Asserting the [$property] property in current scope [{$this->scopeContext->scope}] is a boolean equalling [$expectedValue]."
        );
    }

    /**
     * @Then /^the "([^"]*)" property should be an integer equalling "([^"]*)"$/
     * @param $property
     * @param $expectedValue
     * @throws Exception
     */
    public function thePropertyIsAIntegerEqualling($property, $expectedValue): void
    {
        $payload = $this->requestContext->getScopePayload();
        $actualValue = self::arrayGet($payload, $property);

        $this->thePropertyIsAnInteger($property);

        Assert::assertSame(
            $actualValue,
            (int)$expectedValue,
            "Asserting the [$property] property in current scope [{$this->scopeContext->scope}] is an integer equalling [$expectedValue]."
        );
    }

    /**
     * @Then /^the "([^"]*)" property should be either:$/
     * @param $property
     * @param PyStringNode $options
     * @throws Exception
     */
    public function thePropertyIsEither($property, PyStringNode $options): void
    {
        $payload = $this->requestContext->getScopePayload();
        $actualValue = self::arrayGet($payload, $property);

        $valid = explode("\n", (string)$options);

        Assert::assertContains(
            $actualValue, $valid, sprintf(
                "Asserting the [%s] property in current scope [{$this->scopeContext->scope}] is in array of valid options [%s].",
                $property,
                implode(', ', $valid)
            )
        );
    }

    /**
     * Asserts the the href of the given link name equals this value
     *
     * Since we're using HAL, this would look for something like:
     *      "_links.programmer.href": "/api/programmers/Fred"
     *
     * @Given /^the link "([^"]*)" should exist and its value should be "([^"]*)"$/
     * @param $linkName
     * @param $url
     * @throws Exception
     */
    public function theLinkShouldExistAndItsValueShouldBe($linkName, $url): void
    {
        $this->thePropertyEquals(
            sprintf('_links.%s.href', $linkName),
            $url
        );
    }

    /**
     * @Given /^the embedded "([^"]*)" should have a "([^"]*)" property equal to "([^"]*)"$/
     * @param $embeddedName
     * @param $property
     * @param $value
     * @throws Exception
     */
    public function theEmbeddedShouldHaveAPropertyEqualTo($embeddedName, $property, $value): void
    {
        $this->thePropertyEquals(
            sprintf('_embedded.%s.%s', $embeddedName, $property),
            $value
        );
    }
}

<?php

namespace App\Tests\Behat\Context;

use Behat\Behat\Context\Context;
use Symfony\Component\PropertyAccess\PropertyAccess;

class ReferenceContext implements Context
{
    /** @var array */
    private $references = [];

    /**
     * @Transform /^.*?{{.*?}}.*?$/
     */
    public function castMustaches($string)
    {
        $propertyAccessor = PropertyAccess::createPropertyAccessor();
        return preg_replace_callback("/{{(.*?)}}/", function ($matches) use ($propertyAccessor) {
            $string = trim($matches[1]);
            $explodedPath = explode('.', $string);
            $root = array_shift($explodedPath);
            $value = $this->getReference($root);
            return count($explodedPath) === 0
                ? $value
                : $propertyAccessor->getValue($value, implode('.', $explodedPath));
        }, $string);
    }

    /**
     * @Given /^"([^"]*)" is referenced as "([^"]*)"$/
     * @param $value
     * @param $ref
     */
    public function isReferencedAs($ref, $value): void
    {
        $this->addReference($ref, $value);
    }

    /**
     * @AfterScenario
     */
    public function afterScenario(): void
    {
        $this->references = [];
    }

    public function addReference($ref, $value): void
    {
        $this->references[$ref] = $value;
    }

    private function getReference($ref)
    {
        return $this->references[$ref];
    }
}

<?php

namespace App\Tests\Behat\Context;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\Client;
use App\Tests\Behat\Context\Traits\UtilsTrait;
use Behat\Behat\Context\Context;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Behat\Behat\Tester\Exception\PendingException;
use Behat\Gherkin\Node\PyStringNode;
use Exception;
use GuzzleHttp\Psr7\Request;
use PHPUnit\Framework\Assert;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class RequestContext implements Context
{
    use UtilsTrait;

    /**
     * Payload of the request
     *
     * @var string
     */
    public $requestPayload;

    /**
     * Payload of the response
     *
     * @var string
     */
    public $responsePayload;

    /**
     * The Guzzle client
     *
     * @var Client
     */
    public $client;

    /**
     * The response of the HTTP request
     *
     * @var ResponseInterface
     */
    public $lastResponse;

    /**
     * Headers sent with request
     *
     * @var array[]
     */
    public $requestHeaders = [];

    /**
     * The last request that was used to make the response
     *
     * @var
     */
    public $lastRequest;

    /** @var AuthContext */
    private $authContext;
    /** @var ScopeContext */
    private $scopeContext;
    /** @var ReferenceContext */
    private $refContext;

    public function __construct(KernelInterface $kernel)
    {
        $this->client = $kernel->getContainer()->get('test.api_platform.client');
    }

    /**
     * @param BeforeScenarioScope $scope
     *
     * @BeforeScenario
     */
    public function gatherContexts(BeforeScenarioScope $scope): void
    {
        $environment = $scope->getEnvironment();

        $this->authContext = $environment->getContext(AuthContext::class);
        $this->scopeContext = $environment->getContext(ScopeContext::class);
        $this->refContext = $environment->getContext(ReferenceContext::class);
    }

    /**
     * @BeforeScenario
     */
    public function beforeScenario(): void
    {
        /** Init default content-type */
        $this->requestHeaders["content-type"] = "application/json";
    }

    /**
     * @AfterScenario
     */
    public function afterScenario(): void
    {
        $this->requestHeaders["content-type"] = "";
    }

    /**
     * @Given I have the payload
     * @param PyStringNode $requestPayload
     */
    public function iHaveThePayload(PyStringNode $requestPayload): void
    {
        $this->requestPayload = preg_replace('/\s(?=([^"]*"[^"]*")*[^"]*$)/', '', $requestPayload->getRaw());
        $this->requestPayload = $this->refContext->castMustaches($this->requestPayload);
    }

    /**
     * @When /^I request "(GET|PUT|POST|DELETE|PATCH) ([^"]*)"(?: with payload|$)/
     * @param $httpMethod
     * @param $resource
     * @param PyStringNode|null $payload
     * @throws TransportExceptionInterface
     */
    public function iRequest($httpMethod, $resource, ?PyStringNode $payload = null): void
    {
        if ($payload) {
            $this->iHaveThePayload($payload);
        }
        $method = strtoupper($httpMethod);

        if ($this->authContext->bearer) {
            $this->requestHeaders = array_merge($this->requestHeaders, [
                "Authorization" => sprintf("Bearer %s", $this->authContext->bearer),
            ]);
        }

        $this->lastRequest = new Request(
            $httpMethod,
            $resource,
            $this->requestHeaders,
            $this->requestPayload
        );

        try {
            // Send request
            $this->lastResponse = $this->client->request(
                $method,
                $resource,
                [
                    'headers' => $this->requestHeaders,
                    'body' => $this->requestPayload,
                ]
            );
        } catch (Exception $e) {
            $response = $e->getMessage();

            if ($response === null) {
                throw $e;
            }

            $this->lastResponse = $e->getMessage();
            throw new Exception('Bad response.');
        }
    }

    /**
     * Set before send request
     *
     * @Given /^I set the "([^"]*)" header to be "([^"]*)"$/
     * @param $headerName
     * @param $value
     */
    public function iSetTheHeaderToBe($headerName, $value): void
    {
        $this->requestHeaders[$headerName] = $value;
    }

    /**
     * Test header after request
     *
     * @Given /^the "([^"]*)" header should be "([^"]*)"$/
     * @param $headerName
     * @param $expectedHeaderValue
     * @throws Exception
     */
    public function theHeaderShouldBe($headerName, $expectedHeaderValue): void
    {
        $response = $this->getLastResponse();

//        Assert::assertEquals($expectedHeaderValue, (string)$response->getHeader($headerName));
    }

    /**
     * Test header after request
     *
     * @Given /^the "([^"]*)" header should exist$/
     * @param $headerName
     * @throws Exception
     */
    public function theHeaderShouldExist($headerName): void
    {
        $response = $this->getLastResponse();

//        Assert::assertTrue($response->hasHeader($headerName));
    }

    /**
     * Test status code after request
     *
     * @Then /^the response status code should be (?P<code>\d+)$/
     * @param $statusCode
     * @throws TransportExceptionInterface
     * @throws Exception
     */
    public function theResponseStatusCodeShouldBe($statusCode): void
    {
        $response = $this->getLastResponse();

        Assert::assertEquals($statusCode,
            $response->getStatusCode(),
            sprintf('Expected status code "%s" does not match observed status code "%s"', $statusCode, $response->getStatusCode()));
    }

    /**
     * Checks the response exists and returns it.
     *
     * @return ResponseInterface
     * @throws Exception
     */
    public function getLastResponse(): ResponseInterface
    {
        if (!$this->lastResponse) {
            throw new Exception("You must first make a request to check a response.");
        }

        return $this->lastResponse;
    }


    /**
     * Returns the payload from the current scope within
     * the response.
     *
     * @return mixed
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws Exception
     */
    public function getScopePayload()
    {
        $payload = $this->getResponsePayload();

        if (!$this->scopeContext->scope) {
            return $payload;
        }

        return self::arrayGet($payload, $this->scopeContext->scope, true);
    }

    /**
     * Return the response payload from the current response.
     *
     * @return mixed|string
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws Exception
     * @throws Exception
     * @throws Exception
     */
    public function getResponsePayload()
    {
        $json = json_decode($this->getLastResponse()->getContent(false));
        if (json_last_error() !== JSON_ERROR_NONE) {
            $message = 'Failed to decode JSON body ';

            switch (json_last_error()) {
                case JSON_ERROR_DEPTH:
                    $message .= '(Maximum stack depth exceeded).';
                    break;
                case JSON_ERROR_STATE_MISMATCH:
                    $message .= '(Underflow or the modes mismatch).';
                    break;
                case JSON_ERROR_CTRL_CHAR:
                    $message .= '(Unexpected control character found).';
                    break;
                case JSON_ERROR_SYNTAX:
                    $message .= '(Syntax error, malformed JSON): ' . "\n\n" . $this->getLastResponse()->getContent(false);
                    break;
                case JSON_ERROR_UTF8:
                    $message .= '(Malformed UTF-8 characters, possibly incorrectly encoded).';
                    break;
                default:
                    $message .= '(Unknown error).';
                    break;
            }

            throw new Exception($message);
        }

        $this->responsePayload = $json;
        return $this->responsePayload;
    }

    /**
     * @Given /^the response is referenced as "([^"]*)"$/
     * @param $ref
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function theResponseIsReferencedAs($ref): void
    {
        $this->refContext->addReference($ref, json_decode($this->getLastResponse()->getContent()));
    }
}

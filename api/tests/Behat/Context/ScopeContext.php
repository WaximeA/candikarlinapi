<?php

namespace App\Tests\Behat\Context;

use Behat\Behat\Context\Context;

class ScopeContext implements Context
{
    /**
     * The current scope within the response payload
     * which conditions are asserted against.
     */
    public $scope;

    /**
     * @Then /^scope into the first "([^"]*)" property$/
     */
    public function scopeIntoTheFirstProperty($scope)
    {
        $this->scope = "{$scope}.0";
    }

    /**
     * @Then /^scope into the "([^"]*)" property$/
     */
    public function scopeIntoTheProperty($scope)
    {
        $this->scope = $scope;
    }

    /**
     * @Then /^reset scope$/
     */
    public function resetScope()
    {
        $this->scope = null;
    }
}

Feature: _Offer_

  Background:
    Given the following fixtures files are loaded:
      | users  |
      | offers |

  Scenario: 1. Post offer offline
    When I request "POST /offers" with payload
    """
    {
      "name": "string",
      "companyDescription": "string",
      "offerDescription": "string",
      "startDate": "2020-06-03T12:45:25.244Z",
      "contractType": "string",@
      "workplace": "string"
    }
    """
    Then the response status code should be 401

  Scenario: 2. Post offer as recruiter
    Given I authenticate with user "{{ recruiter_user_1.email }}" and password "{{ recruiter_user_1.plainPassword }}"
    When I request "POST /offers" with payload
    """
    {
      "name": "string",
      "companyDescription": "string",
      "offerDescription": "string",
      "startDate": "2020-06-03T12:45:25.244Z",
      "contractType": "string",
      "workplace": "string"
    }
    """

    Then the response status code should be 201

  Scenario: 3. Post offer as candidate
    Given I authenticate with user "{{ candidate_user_1.email }}" and password "{{ candidate_user_1.plainPassword }}"
    When I request "POST /offers" with payload
      """
      {
        "name": "string",
        "companyDescription": "string",
        "offerDescription": "string",
        "startDate": "2020-06-03T12:45:25.244Z",
        "contractType": "string",
        "workplace": "string"
      }
      """
    Then the response status code should be 403

  Scenario: 4. Post offer missing mandatory field
    Given I authenticate with user "{{ recruiter_user_1.email }}" and password "{{ recruiter_user_1.plainPassword }}"
    When I request "POST /offers" with payload
      """
      {
        "companyDescription": "string",
        "offerDescription": "string",
        "startDate": "2020-06-03T12:45:25.244Z",
        "contractType": "string",
        "workplace": "string"
      }
      """
    Then the response status code should be 500

  Scenario: 5. Post offer wrong date format
    Given I authenticate with user "{{ recruiter_user_1.email }}" and password "{{ recruiter_user_1.plainPassword }}"
    When I request "POST /offers" with payload
    """
    {
      "companyDescription": "string",
      "offerDescription": "string",
      "startDate": "2020-0601",
      "contractType": "string",
      "workplace": "string"
    }
    """
    Then the response status code should be 400

  Scenario: 6. Post offer wrong date format
    Given I authenticate with user "{{ recruiter_user_1.email }}" and password "{{ recruiter_user_1.plainPassword }}"
    When I request "POST /offers" with payload
    """
    {
      "companyDescription": "string",
      "offerDescription": "string",
      "startDate": "2020-0601",
      "contractType": "string",
      "workplace": "string"
    }
    """
    Then the response status code should be 400

  Scenario: 7. Get all offers as candidate
    Given I authenticate with user "{{ candidate_user_1.email }}" and password "{{ candidate_user_1.plainPassword }}"
    When I request "GET /offers"
    Then the response status code should be 200

  Scenario: 8. Get all offers as recruiter
    Given I authenticate with user "{{ recruiter_user_1.email }}" and password "{{ recruiter_user_1.plainPassword }}"
    When I request "GET /offers"
    Then the response status code should be 200

  Scenario: 9. Get all offers as recruiter
    Given I authenticate with user "{{ recruiter_user_1.email }}" and password "{{ recruiter_user_1.plainPassword }}"
    When I request "GET /offers"
    Then the response status code should be 200

  Scenario: 10. Get unknown offer
    Given I authenticate with user "{{ recruiter_user_1.email }}" and password "{{ recruiter_user_1.plainPassword }}"
    When I request "GET /offers/random404"
    Then the response status code should be 404

  Scenario: 11. Get specific offer
    Given I authenticate with user "{{ recruiter_user_1.email }}" and password "{{ recruiter_user_1.plainPassword }}"
    Given I request "GET /offers/{{ offer_1.id }}"
    Then the response status code should be 200

  Scenario: 12. Get specific offer
    Given I authenticate with user "{{ recruiter_user_1.email }}" and password "{{ recruiter_user_1.plainPassword }}"
    Given I request "GET /offers/{{ offer_1.id }}"
    Then the response status code should be 200

  Scenario: 13. Edit all offer fields as creator
    Given I authenticate with user "{{ recruiter_user_1.email }}" and password "{{ recruiter_user_1.plainPassword }}"
    When I request "PUT /offers/{{ offer_1.id }}" with payload
    """
    {
      "name": "string",
      "companyDescription": "string-changed",
      "offerDescription": "string",
      "startDate": "2020-06-03T12:45:25.244Z",
      "contractType": "string",
      "workplace": "string"
    }
    """
    Then the response status code should be 200

  Scenario: 14. Edit all offer fields as random user
    Given I authenticate with user "{{ recruiter_user_2.email }}" and password "{{ recruiter_user_2.plainPassword }}"
    When I request "PUT /offers/{{ offer_8.id }}" with payload
    """
    {
      "name": "string",
      "companyDescription": "string-changed",
      "offerDescription": "string",
      "startDate": "2020-06-03T12:45:25.244Z",
      "contractType": "string",
      "workplace": "string"
    }
    """
    Then the response status code should be 403

  Scenario: 15. Edit single field as recruiter
    Given I authenticate with user "{{ recruiter_user_1.email }}" and password "{{ recruiter_user_1.plainPassword }}"
    When I request "PUT /offers/{{ offer_1.id }}" with payload
    """
    {
      "name": "string",
      "companyDescription": "string-changed",
      "offerDescription": "string",
      "startDate": "2020-06-03T12:45:25.244Z",
      "contractType": "string",
      "workplace": "string"
    }
    """
    Then the response status code should be 200

  Scenario: 16. Get user offers as random user
    Given I authenticate with user "{{ recruiter_user_1.email }}" and password "{{ recruiter_user_1.plainPassword }}"
    When I request "GET /users/{{ recruiter_user_2.id }}/offers"
    Then the response status code should be 200

  Scenario: 17. Delete offer as recruiter owner user @todo refactor
    Given I authenticate with user "{{ offer_1.recruiter.email }}" and password "{{ offer_1.recruiter.plainPassword }}"
    When I request "DELETE /offers/{{ offer_1.id }}"
    Then the response status code should be 204

  Scenario: 18. Delete offer as recruiter not owner user
    Given I authenticate with user "{{ offer_2.recruiter.email }}" and password "{{ offer_2.recruiter.plainPassword }}"
    When I request "DELETE /offers/{{ offer_1.id }}"
    Then the response status code should be 403

  Scenario: 19. Delete offer as candidate user
    Given I authenticate with user "{{ candidate_user_2.email }}" and password "{{ candidate_user_2.plainPassword }}"
    When I request "DELETE /offers/{{ offer_2.id }}"
    Then the response status code should be 403

  Scenario: 20. Delete rando offer as admin user
    Given I authenticate with user "{{ admin_user_1.email }}" and password "{{ admin_user_1.plainPassword }}"
    When I request "DELETE /offers/{{ offer_5.id }}"
    Then the response status code should be 204

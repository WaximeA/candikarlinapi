Feature: _Candidature_

  Background:
    Given the following fixtures files are loaded:
      | users        |
      | offers       |
      | candidatures |

  Scenario: 1. Post candidature offline
    When I request "POST /candidatures" with payload
    """
    {
      "motivationText": "string",
      "salaryClaim": 0,
      "resume": "string",
      "candidate": "/users/{{ candidate_user_1.id }}",
      "offer": "users/{{ offer_1.id }}"
    }
    """
    Then the response status code should be 401

  Scenario: 2. Post candidature as candidate
    Given I authenticate with user "{{ candidate_user_1.email }}" and password "{{ candidate_user_1.plainPassword }}"
    When I request "POST /candidatures" with payload
    """
    {
      "motivationText": "string",
      "salaryClaim": 0,
      "resume": "string",
      "candidate": "/users/{{ candidate_user_1.id }}",
      "offer": "/offers/{{ offer_1.id }}"
    }
    """
    Then the response status code should be 403

  Scenario: 3. Post candidature as recruiter
    Given I authenticate with user "{{ recruiter_user_1.email }}" and password "{{ recruiter_user_1.plainPassword }}"
    When I request "POST /candidatures" with payload
    """
    {
      "motivationText": "string",
      "salaryClaim": 0,
      "resume": "string",
      "candidate": "/users/{{ candidate_user_1.id }}",
      "offer": "/offers/{{ offer_1.id }}"
    }
    """
    Then the response status code should be 201

  Scenario: 4. Post candidature missing mandatory field
    Given I authenticate with user "{{ recruiter_user_1.email }}" and password "{{ recruiter_user_1.plainPassword }}"
    When I request "POST /candidatures" with payload
      """
      {
        "motivationText": "string",
        "salaryClaim": 0,
        "resume": "string",
        "offer": "/offers/{{ offer_1.id }}"
      }
      """
    Then the response status code should be 500

  Scenario: 5. Post candidature wrong IRI format
    Given I authenticate with user "{{ recruiter_user_1.email }}" and password "{{ recruiter_user_1.plainPassword }}"
    When I request "POST /candidatures" with payload
    """
    {
      "motivationText": "string",
      "salaryClaim": 0,
      "resume": "string",
      "candidate": "{{ candidate_user_1.id }}",
      "offer": "/offers/{{ offer_1.id }}"
    }
    """
    Then the response status code should be 400

  Scenario: 6. Post candidature wrong salary format
    Given I authenticate with user "{{ recruiter_user_1.email }}" and password "{{ recruiter_user_1.plainPassword }}"
    When I request "POST /candidatures" with payload
    """
    {
      "motivationText": "string",
      "salaryClaim": "28000",
      "resume": "string",
      "candidate": "/users/{{ candidate_user_1.id }}",
      "offer": "/offers/{{ offer_1.id }}"
    }
    """
    Then the response status code should be 400

  Scenario: 7. Get all candidature as candidate
    Given I authenticate with user "{{ candidate_user_1.email }}" and password "{{ candidate_user_1.plainPassword }}"
    When I request "GET /candidatures"
    Then the response status code should be 403

  Scenario: 8. Get all candidatures as recruiter
    Given I authenticate with user "{{ recruiter_user_1.email }}" and password "{{ recruiter_user_1.plainPassword }}"
    When I request "GET /candidatures"
    Then the response status code should be 403

  Scenario: 9. Get all candidatures as admin
    Given I authenticate with user "{{ admin_user_1.email }}" and password "{{ admin_user_1.plainPassword }}"
    When I request "GET /candidatures"
    Then the response status code should be 200

  Scenario: 10. Get unknown candidature
    Given I authenticate with user "{{ admin_user_1.email }}" and password "{{ admin_user_1.plainPassword }}"
    When I request "GET /candidatures/random404"
    Then the response status code should be 404
#
  Scenario: 11. Get specific candidature as recruiter creator
    Given I authenticate with user "{{ recruiter_user_1.email }}" and password "{{ recruiter_user_1.plainPassword }}"
    Given I request "GET /users/{{ candidature_1.offer.id }}/candidatures"
    Then the response status code should be 200

  Scenario: 12. Get specific candidature as random recruiter @todo refactor code
    Given I authenticate with user "{{ recruiter_user_1.email }}" and password "{{ recruiter_user_1.plainPassword }}"
    Given I request "GET /users/{{ candidate_user_2.id }}/candidatures"
    Then the response status code should be 403

  Scenario: 13. Edit all candidature fields as creator
    Given I authenticate with user "{{ candidate_user_1.email }}" and password "{{ candidate_user_1.plainPassword }}"
    When I request "PUT /candidatures/{{ candidature_1.id }}" with payload
    """
    {
      "motivationText": "string",
      "salaryClaim": 12,
      "resume": "string"
    }
    """
    Then the response status code should be 200

  Scenario: 14. Edit all candidature fields as random candidate user
    Given I authenticate with user "{{ candidate_user_2.email }}" and password "{{ candidate_user_2.plainPassword }}"
    When I request "PUT /candidatures/{{ candidature_1.id }}" with payload
    """
    {
      "motivationText": "string",
      "salaryClaim": 12,
      "resume": "string"
    }
    """
    Then the response status code should be 403

  Scenario: 15. Edit all candidature fields as random recruiter user
    Given I authenticate with user "{{ recruiter_user_2.email }}" and password "{{ recruiter_user_2.plainPassword }}"
    When I request "PUT /candidatures/{{ candidature_1.id }}" with payload
    """
    {
      "motivationText": "string",
      "salaryClaim": 12,
      "resume": "string"
    }
    """
    Then the response status code should be 403

  Scenario: 16. Get user candidatures as random user
    Given I authenticate with user "{{ recruiter_user_1.email }}" and password "{{ recruiter_user_1.plainPassword }}"
    When I request "GET /users/{{ recruiter_user_2.id }}/candidatures"
    Then the response status code should be 200

  Scenario: 17. Delete candidature as recruiter owner user
    Given I authenticate with user "{{ recruiter_user_1.email }}" and password "{{ recruiter_user_1.plainPassword }}"
    When I request "DELETE /candidatures/{{ candidate_user_1.id }}"
    Then the response status code should be 204

  Scenario: 18. Delete candidature as recruiter not owner user
    Given I authenticate with user "{{ recruiter_user_2.email }}" and password "{{ recruiter_user_2.plainPassword }}"
    When I request "DELETE /candidatures/{{ candidature_1.id }}"
    Then the response status code should be 403

  Scenario: 19. Delete candidature as candidate user
    Given I authenticate with user "{{ candidate_user_2.email }}" and password "{{ candidate_user_2.plainPassword }}"
    When I request "DELETE /candidatures/{{ candidature_2.id }}"
    Then the response status code should be 403

  Scenario: 20. Delete random candidature as admin user
    Given I authenticate with user "{{ admin_user_1.email }}" and password "{{ admin_user_1.plainPassword }}"
    When I request "DELETE /candidatures/{{ candidature_5.id }}"
    Then the response status code should be 204

Feature: _Workflow_

  Background:
    Given the following fixtures files are loaded:
      | users  |
      | offers |

  Scenario: Invite a candidate to an offer
    Given I authenticate with user "{{ recruiter_user_1.email }}" and password "{{ recruiter_user_1.plainPassword }}"
    When I request "POST /offers" with payload
    """
        {
          "name": "Poste dev",
          "companyDescription": "ESGI",
          "offerDescription": "Dev JAVA trop bien",
          "startDate": "2020-06-03T12:45:25.244Z",
          "contractType": "CDI",
          "workplace": "Paris"
        }
        """
    Then the response status code should be 201
    And the response is referenced as "current_offer"

    When I request "POST /candidatures" with payload
    """
        {
          "candidate": "/users/{{ candidate_user_1.id }}",
          "offer": "/offers/{{ current_offer.id }}"
        }
        """
    Then the response status code should be 201
    And the response is referenced as "current_candidature"

    Given I authenticate with user "{{ candidate_user_1.email }}" and password "{{ candidate_user_1.plainPassword }}"
    When I request "GET /addCandidate/{{ current_candidature.token }}"
    Then the response status code should be 200

  Scenario: Use app as candidate
    When I request "POST /users" with payload
    """
    {
      "email": "waxime@me.com",
      "roles": [
        "ROLE_CANDIDATE"
      ],
      "password": "waxime",
      "firstname": "Waxime",
      "lastname": "AV",
      "gender": "male",
      "age": 22,
      "address": "10 av gambetta"
    }
    """
    Then the response status code should be 201
    And the response is referenced as "current_user"

    When I request "POST /login" with payload
    """
    {
        "email": "{{ current_user.email }}",
        "password": "{{ current_user.password }}"
    }
    """
    Then the response status code should be 200

    Given I authenticate with user "{{ current_user.email }}" and password "{{ current_user.password }}"
    When I request "GET /offers"
    Then the response status code should be 200
    When I request "GET /offers/{{ offer_1.id }}"
    When I request "PUT /users/{{ current_user.id }}" with payload
    """
    {
      "password": "bestpasswordever"
    }
    """
    Then the response status code should be 200
    When I request "GET /users/{{ current_user.id }}/candidatures"
    Then the response status code should be 200

  Scenario: Use app as hackerman
    When I request "POST /users" with payload
    """
    {
      "email": "hacker@me.com",
      "roles": [
        "ROLE_CANDIDATE"
      ],
      "password": "TestToHackMe",
      "firstname": "Hacker",
      "lastname": "Man",
      "gender": "none",
      "age": 99,
      "address": "10 av gambetta"
    }
    """
    Then the response status code should be 201
    And the response is referenced as "hacker_user"
    Given I authenticate with user "{{ hacker_user.email }}" and password "{{ hacker_user.password }}"

    When I request "GET /users/{{ candidate_user_2.id }}"
    Then the response status code should be 403

    When I request "PUT /users/{{ candidate_user_1.id }}" with payload
    """
    {
      "address": "test hacking"
    }
    """
    Then the response status code should be 403

    When I request "DELETE /users/{{ candidate_user_1.id }}"
    Then the response status code should be 403

    When I request "POST /offers" with payload
      """
      {
        "name": "Offer hack",
        "companyDescription": "hahahaha",
        "offerDescription": "KiddyHat",
        "startDate": "2020-06-03T12:45:25.244Z",
        "contractType": "lol",
        "workplace": "its me Oo-Hakerman-oO"
      }
      """
    Then the response status code should be 403

    When I request "PUT /offers/{{ offer_8.id }}" with payload
    """
    {
      "name": "I want to change your offer",
      "companyDescription": "hehe"
    }
    """
    Then the response status code should be 403

    When I request "DELETE /offers/{{ offer_1.id }}"
    Then the response status code should be 403

    When I request "POST /candidatures" with payload
    """
    {
      "motivationText": "Hack candidature",
      "salaryClaim": 9999999,
      "resume": "I know how to kack",
      "candidate": "/users/{{ hacker_user.id }}",
      "offer": "/offers/{{ offer_1.id }}"
    }
    """
    Then the response status code should be 403

    When I request "GET /candidatures"
    Then the response status code should be 403

    Given I request "GET /users/{{ candidate_user_2.id }}/candidatures"
    Then the response status code should be 403

    When I request "PUT /candidatures/{{ candidature_9.id }}" with payload
    """
    {
      "motivationText": "My motivation is hacking",
      "salaryClaim": 678987678,
      "resume": "I know how to hack"
    }
    """
    Then the response status code should be 403

    When I request "DELETE /candidatures/{{ candidature_2.id }}"
    Then the response status code should be 403

    When I request "DELETE /users/{{ hacker_user.id }}"
    Then the response status code should be 204

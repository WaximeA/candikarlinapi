Feature: _User_

  Background:
    Given the following fixtures files are loaded:
      | users |

  Scenario: 1. login user
    Given "contentType" is referenced as "application/json"
    Given I set the "content-type" header to be "{{ contentType }}"
    When I request "POST /login" with payload
    """
    {
        "email": "test@test.com",
        "password": "test"
    }
    """

    Then the response status code should be 200

  Scenario: 2. Login unexisting user
    When I request "POST /login" with payload
    """
    {
        "email": "toto@toto.com",
        "password": "toto"
    }
    """
    Then the response status code should be 401

  Scenario: 3. Login wrong password
    When I request "POST /login" with payload
    """
    {
        "email": "test@test.com",
        "password": "toto"
    }
    """
    Then the response status code should be 401

  Scenario: 4. Register user
    When I request "POST /users" with payload
    """
    {
      "email": "string@string.com",
      "roles": [
        "ROLE_CANDIDATE"
      ],
      "password": "string",
      "firstname": "string",
      "lastname": "string",
      "gender": "string",
      "age": 1,
      "address": "string"
    }
    """
    Then the response status code should be 201

  Scenario: 5. Register user already exist
    When I request "POST /users" with payload
    """
    {
      "email": "test@test.com",
      "roles": [
        "ROLE_CANDIDATE"
      ],
      "password": "test",
      "firstname": "string",
      "lastname": "string",
      "gender": "string",
      "age": 1,
      "address": "string"
    }
    """
    Then the response status code should be 500

  Scenario: 6. Register with uncorrect email
    When I request "POST /users" with payload
    """
    {
      "email": "string@string",
      "roles": [
        "ROLE_CANDIDATE"
      ],
      "password": "test",
      "firstname": "string",
      "lastname": "string",
      "gender": "string",
      "age": 1,
      "address": "string"
    }
    """
    Then the response status code should be 400

  Scenario: 7. Get all users as admin user
    Given I authenticate with user "{{ admin_user_1.email }}" and password "{{ admin_user_1.plainPassword }}"
    When I request "GET /users"
    Then the response status code should be 200

  Scenario: 8. Get all users not sign in @todo refactor code
    When I request "GET /users"
    Then the response status code should be 401

  Scenario: 9. Get unknown user
    Given I authenticate with user "{{ admin_user_1.email }}" and password "{{ admin_user_1.plainPassword }}"
    When I request "GET /users/random404"
    Then the response status code should be 404

  Scenario: 10. Edit user as good user
    Given I authenticate with user "{{ candidate_user_1.email }}" and password "{{ candidate_user_1.plainPassword }}"
    When I request "GET /users/{{ candidate_user_1.id }}"
    Then the response status code should be 200

  Scenario: 11. Edit user as random user
    Given I authenticate with user "{{ candidate_user_1.email }}" and password "{{ candidate_user_1.plainPassword }}"
    When I request "GET /users/{{ candidate_user_2.id }}"
    Then the response status code should be 403

  Scenario: 12. Edit user as current user
    Given I authenticate with user "{{ candidate_user_1.email }}" and password "{{ candidate_user_1.plainPassword }}"
    When I request "PUT /users/{{ candidate_user_1.id }}" with payload
    """
    {
      "address": "string 2"
    }
    """
    Then the response status code should be 200

  Scenario: 13. Edit user as wrong user
    Given I authenticate with user "{{ candidate_user_2.email }}" and password "{{ candidate_user_2.plainPassword }}"
    When I request "PUT /users/{{ candidate_user_1.id }}" with payload
    """
    {
      "address": "string 2"
    }
    """
    Then the response status code should be 403

  Scenario: 14. Edit user but with wrong format
    Given I authenticate with user "{{ candidate_user_1.email }}" and password "{{ candidate_user_1.plainPassword }}"
    When I request "PUT /users/{{ candidate_user_1.id }}" with payload
    """
    {
      "email": "string@string"
    }
    """
    Then the response status code should be 400

  Scenario: 15. Delete user as current user
    Given I authenticate with user "{{ candidate_user_1.email }}" and password "{{ candidate_user_1.plainPassword }}"
    When I request "DELETE /users/{{ candidate_user_1.id }}"
    Then the response status code should be 204

  Scenario: 16. Delete user as wrong user
    Given I authenticate with user "{{ candidate_user_2.email }}" and password "{{ candidate_user_2.plainPassword }}"
    When I request "DELETE /users/{{ candidate_user_1.id }}"
    Then the response status code should be 403

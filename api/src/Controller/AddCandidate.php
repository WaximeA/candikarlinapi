<?php

namespace App\Controller;

use App\Entity\Candidature;
use App\Repository\CandidatureRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;

class AddCandidate
{
    public function __invoke(Request $data, EntityManagerInterface $entityManager)
    {
        $candidatureRepository = $entityManager->getRepository(Candidature::class);

        /**
         * @var CandidatureRepository $candidatureRepository
         */
        $candidature = $candidatureRepository->findOneByToken($data->get('token'));
        $candidature->setStatus(Candidature::OPENED_STATUS);
        $entityManager->persist($candidature);
        $entityManager->flush();
        return $candidature;
    }
}

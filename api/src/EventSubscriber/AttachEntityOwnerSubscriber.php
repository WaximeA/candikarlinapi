<?php

namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\Candidature;
use App\Entity\Offer;
use App\Entity\User;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class AttachEntityOwnerSubscriber
 *
 * @package App\EventSubscriber
 */
final class AttachEntityOwnerSubscriber implements EventSubscriberInterface
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * {@inheritDoc}
     *
     * @return array[]
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => ['attachEntityOwner', EventPriorities::PRE_WRITE],
        ];
    }

    /**
     * Attach entity owner
     *
     * @param ViewEvent $event
     *
     * @return void
     */
    public function attachEntityOwner(ViewEvent $event): void
    {
        $entity      = $event->getControllerResult();
        $method      = $event->getRequest()->getMethod();
        $classes     = [Offer::class];
        $entityClass = get_class($entity);

        if (!in_array($entityClass, $classes) || Request::METHOD_POST !== $method) {
            return;
        }

        $token = $this->tokenStorage->getToken();

        if (!$token) {
            return;
        }

        $owner = $token->getUser();

        if (!$owner instanceof User) {
            return;
        }

        if ($entity instanceof Offer) {
            $entity->setRecruiter($owner);
        }
    }
}

<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\AddCandidate;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     collectionOperations={
 *         "get"={"security"="is_granted('ROLE_ADMIN')"},
 *         "post"={
 *              "security"="is_granted('ROLE_ADMIN') or is_granted('ROLE_RECRUITER')",
 *              "denormalization_context"={"groups"={"candidature_post_write"}},
 *          },
 *     },
 *     itemOperations={
 *         "get"={"security"="is_granted('ROLE_ADMIN') or (is_granted('ROLE_CANDIDATE') and object.candidate == user) or (is_granted('ROLE_RECRUITER') and object.offer.recruiter == user)"},
 *         "put"={"security"="is_granted('ROLE_ADMIN') or (is_granted('ROLE_CANDIDATE') and object.candidate == user)"},
 *         "delete"={"security"="is_granted('ROLE_ADMIN') or (is_granted('ROLE_CANDIDATE') and object.candidate == user)"},
 *         "patch"={"security"="is_granted('ROLE_ADMIN') or (is_granted('ROLE_CANDIDATE') and object.candidate == user)"},
 *         "addCandidate"={
 *         "method"="GET",
 *         "path"="/addCandidate/{token}",
 *         "controller"=AddCandidate::class,
 *         "defaults"={"_api_receive"=false},
 *         "openapi_context"= {
 *             "parameters"= {
 *                 {
 *                    "name": "token",
 *                    "in": "path",
 *                    "type": "string",
 *                    "required": true
 *                 }
 *             }
 *         }
 *     }
 *     },
 * )
 *
 * @ORM\Entity(repositoryClass="App\Repository\CandidatureRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Candidature
{
    /** @var string CREATED_STATUS */
    public const CREATED_STATUS = 'created';
    /** @var string OPENED_STATUS */
    public const OPENED_STATUS = 'opened';
    /** @var string PENDING_STATUS */
    public const PENDING_STATUS = 'pending';
    /** @var string FIXED_APPOINTMENT_STATUS */
    public const FIXED_APPOINTMENT_STATUS = 'fixed_appointment';
    /** @var string ACCEPTED_STATUS */
    public const ACCEPTED_STATUS = 'accepted';
    /** @var string REFUSED_STATUS */
    public const REFUSED_STATUS = 'refused';

    /**
     * Get allowed status
     *
     * @return array|string[]
     */
    public static function getAllowedStatus(): array
    {
        return [self::CREATED_STATUS, self::OPENED_STATUS, self::PENDING_STATUS, self::FIXED_APPOINTMENT_STATUS, self::ACCEPTED_STATUS, self::REFUSED_STATUS];
    }

    /**
     * @ORM\PrePersist()
     */
    public function generateToken()
    {
        $this->token = uuid_create(UUID_TYPE_RANDOM);
    }

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"candidature_post_write","user_get_read","offer_get_read"})
     */
    private $motivationText;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"candidature_post_write","user_get_read","offer_get_read"})
     */
    private $salaryClaim;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"candidature_post_write","user_get_read","offer_get_read"})
     */
    private $resume;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Choice(callback="getAllowedStatus")
     * @Groups({"user_get_read","offer_get_read"})
     */
    private $status = self::CREATED_STATUS;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="candidatures")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"candidature_post_write","offer_get_read"})
     */
    public $candidate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Offer", inversedBy="candidatures")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"candidature_post_write","user_get_read"})
     */
    public $offer;

    /**
     * @ORM\Column(type="string")
     */
    private $token;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMotivationText(): ?string
    {
        return $this->motivationText;
    }

    public function setMotivationText(string $motivationText): self
    {
        $this->motivationText = $motivationText;

        return $this;
    }

    public function getSalaryClaim(): ?int
    {
        return $this->salaryClaim;
    }

    public function setSalaryClaim(int $salaryClaim): self
    {
        $this->salaryClaim = $salaryClaim;

        return $this;
    }

    public function getResume(): ?string
    {
        return $this->resume;
    }

    public function setResume(string $resume): self
    {
        $this->resume = $resume;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCandidate(): ?User
    {
        return $this->candidate;
    }

    public function setCandidate(?User $candidate): self
    {
        $this->candidate = $candidate;

        return $this;
    }

    public function getOffer(): ?Offer
    {
        return $this->offer;
    }

    public function setOffer(?Offer $offer): self
    {
        $this->offer = $offer;

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(string $token): self
    {
        $this->token = $token;

        return $this;
    }
}
